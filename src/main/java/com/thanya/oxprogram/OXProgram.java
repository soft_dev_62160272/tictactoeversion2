package com.thanya.oxprogram;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Thanyarak Namwong
 */
public class OXProgram {

    static char winner = '-';
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}
    };
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game.");

    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table.length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }

    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {

            System.out.println("Plese input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;

            }
            System.out.println("Error: table at row and col is not emtry!!!");
        }
//        showTable();
    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void showcheckWin() {
        checkCol();
        checkRow();
        checkX();
        checkDraw();

    }

    static void checkX() {
        if(table[0][2] == player|| table[0][0] == player){
            if(table[2][2] == player|| table[1][1] == player){
                if(table[2][0] == player||table[2][2] == player){
                    isFinish = true;
                    winner = player;
                }
            }
        }
//        if(table[0][2] == 'O'|| table[0][0] == 'O'){
//            if(table[2][2] == 'O'|| table[1][1] == 'O'){
//                if(table[2][0] == 'O'||table[2][2] == 'O'){
//                    isFinish = true;
//                    winner = player;
//                }
//            }
//        }
//        if(table[0][0] == 'X'){
//            if(table[1][1] == 'X'){
//                if(table[2][2] == 'X'){
//                    isFinish = true;
//                    winner = player;
//                }
//            }
//        }
//        if(table[0][2] == 'X'){
//            if(table[2][2] == 'X'){
//                if(table[2][0] == 'X'){
//                    isFinish = true;
//                    winner = player;
//                }
//            }
//        }
    }


    static void checkDraw() {
        for (int col = 0; col < 3; col++) {
            for (int row = 0; row < 3; row++) {
                if (table[row][col] == '-') {
                    return;
                }
            }
        }
        isFinish = true;

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            showTable();
            System.out.println("Draw!!");
            
        } else {
            showTable();
            System.out.println("Player " + winner + " Win....");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye ....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            showcheckWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();

    }
}
